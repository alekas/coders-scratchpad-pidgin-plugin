

typedef struct
{
	PidginConversation *gtkconv;
	GtkWidget *gtkconv_vbox;
	GtkWidget *gtkconv_parent;
	GtkSourceView *sourceView;
	GtkSourceLanguageManager *languageManager;
	GtkSourceBuffer *sourceBuffer;
	GtkSourceStyleSchemeManager *styleManager;
	GtkToggleButton *toggleButton;
	GtkHPaned *hpaned;
} ScratchpadInfo;