
#ifdef HAVE_CONFIG_H
# include <config.h>
#endif

/* config.h may define PURPLE_PLUGINS; protect the definition here so that we
 * don't get complaints about redefinition when it's not necessary. */
#ifndef PURPLE_PLUGINS
# define PURPLE_PLUGINS
#endif

#include <glib.h>

/* This will prevent compiler errors in some instances and is better explained in the
 * how-to documents on the wiki */
#ifndef G_GNUC_NULL_TERMINATED
# if __GNUC__ >= 4
#  define G_GNUC_NULL_TERMINATED __attribute__((__sentinel__))
# else
#  define G_GNUC_NULL_TERMINATED
# endif
#endif

#include <notify.h>
#include <plugin.h>
#include <internal.h>
#include <version.h>
#include "pidgin.h"
#include "gtkconv.h"
#include "gtkplugin.h"
#include "gtkdebug.h"
#include <windows.h>
#include "debug.h"
#include <gtksourceview/gtksourceview.h>
#include <gtksourceview/gtksourcelanguagemanager.h>
#include <gtksourceview/gtksourcestyleschememanager.h>
#include <coders_scratchpad.h>

#define PLUGIN_ID "gtk-spadusa-coders_scratchpad"
#define CODE_PREFIX "#gtk-spadusa-coders_scratchpad#"

/* we're adding this here and assigning it in plugin_load because we need
 * a valid plugin handle for our call to purple_notify_message() in the
 * plugin_action_test_cb() callback function */
PurplePlugin *coders_scratchpad_plugin = NULL;

GList *conv_info;

static void language_selected(GtkWidget *widget, gpointer pinfo)
{
	ScratchpadInfo *info = pinfo;
	
	gchar *language_id = gtk_combo_box_get_active_text(GTK_COMBO_BOX(widget));
	GtkSourceBuffer *sourceBuffer = info->sourceBuffer;
	//GtkSourceLanguageManager *languageManager = (GtkSourceLanguageManager*)info->languageManager;
	GtkSourceLanguage *language = gtk_source_language_manager_get_language(info->languageManager, language_id);
	purple_debug_info("gtk-spadusa-memory-debug", "info add language_selected: %p\n", info);
	purple_debug_info("gtk-spadusa-memory-debug", "languageManager: %p\n", info->languageManager);
	if(language!=NULL)
	{
		purple_debug_info(PLUGIN_ID, "Setting current language to: %s\n", language_id);
		gtk_source_buffer_set_language(GTK_SOURCE_BUFFER(sourceBuffer), language);
	}
}

static void send_code_payload(ScratchpadInfo *info, char *message)
{
	PurpleConnection *connection = purple_conversation_get_gc(info->gtkconv->active_conv);
	const char *convName = purple_conversation_get_name(info->gtkconv->active_conv);
	
	GString *to_send = g_string_new("");
	g_string_append_printf(to_send, "%s %s", CODE_PREFIX, purple_base64_encode((guchar*)message, strlen(message)));
	serv_send_im(connection, convName, to_send->str, PURPLE_MESSAGE_SEND);
}

static void send_code(GtkWidget *widget, gpointer pinfo)
{
	ScratchpadInfo *info = pinfo;
	
	GtkSourceBuffer *sourceBuffer = info->sourceBuffer;
	char *text;
	GtkTextIter *end_iter=malloc(sizeof(GtkTextIter));
	GtkTextIter *start_iter=malloc(sizeof(GtkTextIter));
	gtk_text_buffer_get_end_iter(GTK_TEXT_BUFFER(sourceBuffer), end_iter);
	gtk_text_buffer_get_start_iter(GTK_TEXT_BUFFER(sourceBuffer), start_iter);
	if(start_iter != NULL && end_iter!=NULL)
	{
		text = gtk_text_buffer_get_text(GTK_TEXT_BUFFER(sourceBuffer), start_iter, end_iter, FALSE);
		purple_debug_info(PLUGIN_ID, "Source text: %s\n", text);
		send_code_payload(info, text);
	}
}

static void togglePane(GtkWidget *widget, gpointer pinfo)
{
	ScratchpadInfo *info = pinfo;
	purple_debug_info(PLUGIN_ID, "info add1: %p\n", info);
	purple_debug_info(PLUGIN_ID, "info hpaned: %p\n", &info->hpaned);
	
	if(GTK_TOGGLE_BUTTON(widget)->active)
	{
		gtk_paned_set_position(GTK_PANED(info->hpaned), 400);
	}
	else
	{
		gtk_paned_set_position(GTK_PANED(info->hpaned), INT_MAX);
	}
		
}

static void create_scratchpad_toggle_button(PidginConversation *gtkconv, ScratchpadInfo* info)
{
	GtkWidget *toggleButton;
	GtkWidget *hbox;
	GtkWidget *image;
	GtkWidget *label;
	
	gchar *file_path = g_build_filename(purple_user_dir(), "plugins", "coders_scratchpad", "icons", "edit-code.png", NULL);
	toggleButton = gtk_toggle_button_new();
	gtk_button_set_relief(GTK_BUTTON(toggleButton), GTK_RELIEF_NONE);
	hbox = gtk_hbox_new(FALSE, 0);
	image = gtk_image_new_from_file(file_path);
	g_free(file_path);

	gtk_box_pack_start(GTK_BOX(hbox), image, FALSE, FALSE, 0);
	label = gtk_label_new("Code");
	gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 0);
	gtk_container_add(GTK_CONTAINER(toggleButton), hbox);
	gtk_box_pack_start(GTK_BOX(gtkconv->toolbar), toggleButton, FALSE, FALSE, 0);
	gtk_widget_show_all(toggleButton);
	
	purple_debug_info("gtk-spadusa-memory-debug", "info add create_scratchpad_toggle_button: %p\n", info);
	g_signal_connect(G_OBJECT(toggleButton), "toggled", G_CALLBACK(togglePane), info);
}

static void setup_scratchpad_window(PidginConversation *gtkconv)
{
	GtkWidget *scrollableWindow;
	PangoFontDescription *fontDesc;
	gchar* language_path[2] = {NULL, NULL};
	gchar* style_path;
	
	GtkWidget *vbox;
	GtkWidget *hbox;
	GtkWidget *languageBox;
	GtkWidget *languageLabel;
	GtkWidget *codeSendButton;
	const gchar* const * id_ptr;
	int i;
	char* is_setup;

	
	ScratchpadInfo *info = g_malloc(sizeof(ScratchpadInfo));
	
	purple_debug_info("gtk-spadusa-memory-debug", "info add setup_scratchpad_window: %p\n", &info);

	info->gtkconv_vbox = gtk_widget_get_parent(gtk_widget_get_parent(gtk_widget_get_parent(gtk_widget_get_parent(gtkconv->imhtml))));
	info->gtkconv_parent = gtk_widget_get_parent(info->gtkconv_vbox);
	
	is_setup = (char*) g_object_get_data(G_OBJECT(gtkconv->lower_hbox), "sourceview_setup");	
	purple_debug_info(PLUGIN_ID, "Is setup? %s\n", is_setup);
	if(is_setup!=NULL)
	{
		purple_debug_info(PLUGIN_ID, "Window already constructed");
		return;
	}
	info->gtkconv = gtkconv;
	
	
	language_path[0] = g_build_filename(purple_user_dir(), "plugins", "coders_scratchpad", "languages", NULL);
	style_path = g_build_filename(purple_user_dir(), "plugins", "coders_scratchpad", "styles", NULL);
	
	info->hpaned = GTK_HPANED(gtk_hpaned_new());

	purple_debug_info("gtk-spadusa-memory-debug", "hpaned: %p\n", &info->hpaned);
	
	gtk_paned_set_gutter_size(GTK_PANED(info->hpaned), 15);
	gtk_paned_set_position(GTK_PANED(info->hpaned), INT_MAX);
	
	//left side of the pane
	//add a reference so it doesn't get destroyed
	g_object_ref(G_OBJECT(info->gtkconv_vbox));
	//remove container
	gtk_container_remove(GTK_CONTAINER(info->gtkconv_parent), GTK_WIDGET(info->gtkconv_vbox));
	gtk_paned_pack1(GTK_PANED(info->hpaned), GTK_WIDGET(info->gtkconv_vbox), TRUE, TRUE);
	
	//right side
	hbox = gtk_hbox_new(FALSE, 0);
	vbox = gtk_vbox_new(FALSE, 0);
	
	scrollableWindow = gtk_scrolled_window_new(NULL, NULL);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollableWindow), GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
	
	languageLabel = gtk_label_new("Syntax Highlighting: ");
	codeSendButton = gtk_button_new_with_label("Send");
	languageBox = gtk_combo_box_new_text(); 
	info->languageManager = GTK_SOURCE_LANGUAGE_MANAGER(gtk_source_language_manager_new());
	purple_debug_info("gtk-spadusa-memory-debug", "languageManager: %p\n", &info->languageManager);
	
	for (i=0; language_path[i]!=NULL; i++)
	{
		purple_debug_info(PLUGIN_ID, "Setting language search path: %s\n", language_path[i]);
	}
	
	gtk_source_language_manager_set_search_path(GTK_SOURCE_LANGUAGE_MANAGER(info->languageManager), language_path);
	
	for (id_ptr = gtk_source_language_manager_get_language_ids(GTK_SOURCE_LANGUAGE_MANAGER(info->languageManager)); id_ptr != NULL && *id_ptr != NULL; id_ptr++)
	{
		purple_debug_info(PLUGIN_ID, "Language: %s\n", *id_ptr);
		gtk_combo_box_append_text(GTK_COMBO_BOX(languageBox), *id_ptr);
	}
	
	info->sourceBuffer = gtk_source_buffer_new(NULL);
	gtk_source_buffer_set_highlight_syntax(GTK_SOURCE_BUFFER(info->sourceBuffer), TRUE);
	
	info->styleManager = gtk_source_style_scheme_manager_new();
	gtk_source_style_scheme_manager_append_search_path(GTK_SOURCE_STYLE_SCHEME_MANAGER(info->styleManager), style_path);
	i=0;
	for(id_ptr = gtk_source_style_scheme_manager_get_scheme_ids(GTK_SOURCE_STYLE_SCHEME_MANAGER(info->styleManager)); id_ptr != NULL && *id_ptr != NULL; id_ptr++)
	{
		if(i==0)
		{
			GtkSourceStyleScheme *scheme = gtk_source_style_scheme_manager_get_scheme(GTK_SOURCE_STYLE_SCHEME_MANAGER(info->styleManager), *id_ptr);
			purple_debug_info(PLUGIN_ID, "Active Style: %s\n", *id_ptr);
			gtk_source_buffer_set_style_scheme(GTK_SOURCE_BUFFER(info->sourceBuffer), scheme);
		}
		purple_debug_info(PLUGIN_ID, "Style: %s\n", *id_ptr);
		i++;
	}
		
	info->sourceView = GTK_SOURCE_VIEW(gtk_source_view_new_with_buffer(GTK_SOURCE_BUFFER(info->sourceBuffer)));
	gtk_source_view_set_show_line_numbers(GTK_SOURCE_VIEW(info->sourceView), TRUE);
	
	fontDesc = pango_font_description_from_string("mono 8");
	gtk_widget_modify_font(GTK_WIDGET(info->sourceView), fontDesc);
	pango_font_description_free(fontDesc);
	gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(languageLabel), FALSE, FALSE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(languageBox), TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(hbox), GTK_WIDGET(codeSendButton), FALSE, FALSE, 0);
	
	gtk_container_add(GTK_CONTAINER(scrollableWindow), GTK_WIDGET(info->sourceView));
	
	gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(scrollableWindow), TRUE, TRUE, 0);
	gtk_box_pack_start(GTK_BOX(vbox), GTK_WIDGET(hbox), FALSE, FALSE, 0);
	
	gtk_paned_pack2(GTK_PANED(info->hpaned), GTK_WIDGET(vbox), TRUE, TRUE);
	
	g_signal_connect(G_OBJECT(languageBox), "changed", G_CALLBACK(language_selected), info);
	g_signal_connect(G_OBJECT(codeSendButton), "clicked", G_CALLBACK(send_code), info);
	
	gtk_widget_show(GTK_WIDGET(info->hpaned));
	//gtk_widget_show(hbox);
	gtk_widget_show_all(vbox);
	gtk_box_pack_start(GTK_BOX(info->gtkconv_parent), GTK_WIDGET(info->hpaned), TRUE, TRUE, 0);

	g_object_set_data_full(G_OBJECT(gtkconv->lower_hbox), "sourceview_setup", g_strdup("true"), (GDestroyNotify) g_free);
	
	conv_info = g_list_append(conv_info, info);
	create_scratchpad_toggle_button(gtkconv, info);
}

static ScratchpadInfo* getInfoFromGTKConv(PidginConversation *gtkconv)
{
	GList *l;
	ScratchpadInfo *current_info = NULL;
	guint i;

	i = 0;
	for (l = conv_info; l != NULL; l = l->next)
	{
		current_info = l->data;
		if (gtkconv == current_info->gtkconv)
		{
			return (ScratchpadInfo *)g_list_nth_data(conv_info, i);
		}
		i++;
	}
	return NULL;
}

static void callback_conversation_displayed(PidginConversation *gtkconv)
{
	setup_scratchpad_window(gtkconv);
}

static void
intercept_sent(PurpleAccount *account, const char *who, char **message, void* pData)
{
	if (message == NULL || *message == NULL || **message == '\0')
		return;

	if (0 == strncmp(*message, CODE_PREFIX, strlen(CODE_PREFIX)))
	{
		purple_debug_misc(PLUGIN_ID, "Sent code Message: %s\n", *message);
	}
}

static gboolean
intercept_received(PurpleAccount *account, char **sender, char **message, PurpleConversation *conv, int *flags)
{
	ScratchpadInfo *info = getInfoFromGTKConv(PIDGIN_CONVERSATION(conv));
	char *msg = purple_markup_strip_html(*message);
	purple_debug_info(PLUGIN_ID, "Received Message from: %s\n", *sender);
	if(info!=NULL)
	{
		purple_debug_info(PLUGIN_ID, "Info address: %p\n", (void *)info);
		purple_debug_info(PLUGIN_ID, "Message: %s=?=%s\n", msg, CODE_PREFIX);
		if(0==strncmp(msg, CODE_PREFIX, strlen(CODE_PREFIX)))
		{
			gsize length;
			char *prefix = malloc(sizeof(char)*strlen(CODE_PREFIX));
			char *encoded_msg = malloc(sizeof(char*)*(strlen(*message)-strlen(CODE_PREFIX)));
			
			if(sscanf(msg, "%s%s", prefix, encoded_msg)==2)
			{
				char *code_message = (char*)purple_base64_decode(encoded_msg, &length);
				purple_debug_misc(PLUGIN_ID, "Received code message: %s - %s\n", *message, code_message);
				gtk_text_buffer_set_text(GTK_TEXT_BUFFER(info->sourceBuffer), code_message, strlen(code_message));
				return TRUE;
			}
		}
	}
	return FALSE;
}

static gboolean plugin_load(PurplePlugin *plugin)
{
	GList *convs = purple_get_conversations();
	void *gtk_conv_handle = pidgin_conversations_get_handle();
	void *conv_list_handle = purple_conversations_get_handle();
	//GtkSourceLanguageManager *lm;
	//GtkSourceStyleSchemeManager *sm;
	//GtkSourceBuffer *buffer;
	
	pidgin_debug_window_show();

	purple_signal_connect(gtk_conv_handle, "conversation-displayed", plugin,
	                      PURPLE_CALLBACK(callback_conversation_displayed), NULL);

	while (convs) {

		PurpleConversation *conv = (PurpleConversation *)convs->data;

		/* Setup button to toggle scratchpad */
		if (PIDGIN_IS_PIDGIN_CONVERSATION(conv)) {
			setup_scratchpad_window(PIDGIN_CONVERSATION(conv));
		}

		convs = convs->next;
	}
	
	purple_signal_connect(conv_list_handle, "sending-im-msg", plugin, PURPLE_CALLBACK(intercept_sent), NULL);
	purple_signal_connect(conv_list_handle, "receiving-im-msg", plugin, PURPLE_CALLBACK(intercept_received), NULL);

	return TRUE;
}

/* For specific notes on the meanings of each of these members, consult the C Plugin Howto
 * on the website. */
static PurplePluginInfo info = {
	PURPLE_PLUGIN_MAGIC,
	PURPLE_MAJOR_VERSION,
	PURPLE_MINOR_VERSION,
	PURPLE_PLUGIN_STANDARD,
	NULL,
	0,
	NULL,
	PURPLE_PRIORITY_DEFAULT,

	PLUGIN_ID,
	"Coder's Scratchpad!",
	"1.1",

	"Coder's Scratchpad",
	"A plug for coders to be able to communicate about code better",
	"Andreas Lekas <andreaslekas@gmail.com>",
	"http://andreaslekas.com",


	plugin_load,
	NULL,
	NULL,
 
	NULL,
	NULL,
	NULL,
	NULL,		
	NULL,
	NULL,
	NULL,
	NULL
};

static void
init_plugin (PurplePlugin * plugin)
{
	purple_debug_info(PLUGIN_ID, "Plugin Init\n");
	purple_debug_info(PLUGIN_ID, "Path: %s", g_build_filename(purple_home_dir(), "plugins", "coders_scratchpad", "languages", NULL));
	purple_prefs_add_none("/plugins/gtk/coders_scratchpad/");
	purple_prefs_add_path("/plugins/gtk/coders_scratchpad/language_path", g_build_filename(purple_user_dir(), "plugins", "coders_scratchpad", "languages", NULL));
	purple_prefs_add_path("/plugins/gtk/coders_scratchpad/styles_path", g_build_filename(purple_user_dir(), "plugins", "coders_scratchpad", "styles", NULL));
}

PURPLE_INIT_PLUGIN (coders_scratchpad, init_plugin, info)
